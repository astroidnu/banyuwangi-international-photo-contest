<?php 
class frontend_model_user extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->helper('text');
    }

    public function get_detail_user($id)
    {
        $data = array();
        $this->db->where('ID_Contest', $id);

        $q = $this->db->get('tb_contestan');

        if(($q->num_rows() > 0))
        {
            foreach ($q->result_array() as $row)
            {
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
     function update_user($id, $username, $passwd, $singlefamily, $qtyfamily,$phonenumber)
    {
        $newdata = array(
            'Username' => $username,
            'Password'  => $passwd,
            'SinggleFamily' => $singlefamily,
            'QtyFamily' => $qtyfamily,
            'MobileNumber' => $phonenumber
        );
        
        $this->db->where('ID_Contest', $id);
        $q = $this->db->update('tb_contestan', $newdata);
        
        return $q;
    }
         function add_user_model($idcard, $username, $passwd, $firstname, $lastname,$date, $gender_baru,$family_baru, $qtyfamily,$phonenumber)
    {
        $newdata = array(
            'ID_Card' => $idcard,
            'Username' => $username,
            'Password'  => $passwd,
            'SinggleFamily' => $family_baru,
            'QtyFamily' => $qtyfamily,
            'MobileNumber' => $phonenumber,
            'Gender' => $gender_baru,
            'FirstName' => $firstname,
            'LastName' => $lastname,
            'BirthDate' => $date
        );
        
        $q = $this->db->insert('tb_contestan', $newdata);
        
        return $q;
    }
}
?>
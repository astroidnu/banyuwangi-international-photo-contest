<div class="container">
      <div class="row">
        <div class="col-md-12" >
          <div class="panel panel-info">
            <div class="panel-heading">
               <?php if(count($users) > 0){ foreach ($users as $user) { ?>
              <h3 class="panel-title"><?php echo $user['FirstName']; echo' ';echo $user['LastName'];?></h3>
                 <?php }} ?>      
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="<?php echo base_url()?>assets/images/img.jpg" class="img-circle img-responsive"> </div>
                
                <!--<div class="col-xs-10 col-sm-10 hidden-md hidden-lg"> <br>
                  <dl>
                    <dt>DEPARTMENT:</dt>
                    <dd>Administrator</dd>
                    <dt>HIRE DATE</dt>
                    <dd>11/12/2013</dd>
                    <dt>DATE OF BIRTH</dt>
                       <dd>11/12/2013</dd>
                    <dt>GENDER</dt>
                    <dd>Male</dd>
                  </dl>
                </div>-->
                <div class=" col-md-9 col-lg-9 "> 
                   <?php echo form_open('ipsite/home/update_user'); ?>
                   <form role="form" action="" method="post">
                  <table class="table table-user-information">
                    <tbody>
                       <tr>
                        <td>ID Card</td>
                        <td><input type="text" class="form-control col-md-7 col-xs-12" name="idcard" id="idcard" value="<?php echo $user['ID_Card']?>"  /></td>
                      </tr>
                      <tr>
                        <td>Username</td>
                        <td><input type="text" class="form-control col-md-7 col-xs-12" name="username" id="username" value="<?php echo $user['Username']?>"/></td>
                      </tr>
                         <tr>
                        <td>Password</td>
                        <td><input type="password" class="form-control col-md-7 col-xs-12" name="passwd" id="passwd" value="<?php echo $user['Password']?>"/></td>
                      </tr>
                      <tr>
                        <td>Date of Birth</td>
                        <td><input type="text" class="form-control col-md-7 col-xs-12" name="date" id="idcard" value="<?php echo $user['BirthDate']?>" /></td>
                      </tr>
                      <tr>
                        <td>Gender</td>
                        <td><input type="text" class="form-control col-md-7 col-xs-12" name="gender" id="gender" value="<?php if($user['Gender']==1){
                          echo "Male";}else{
                            echo "Female";}?>" /></td>
                      </tr>
                      <tr>
                         <tr>
                        <td>Single Family</td>
                        <td><input type="text" class="form-control col-md-7 col-xs-12" name="singlefamily" id="singlefamily" value="<?php echo $user['SinggleFamily']?>"/></td>
                      </tr>
                      <tr>
                         <tr>
                        <td>Qty Family</td>
                        <td><input type="text" class="form-control col-md-7 col-xs-12" name="qtyfamily" id="qtyfamily" value="<?php echo $user['QtyFamily']?>"/></td>
                      </tr>
                      <tr>
                        <td>Email</td>
                        <td><a href="mailto:info@support.com"><input type="text" class="form-control col-md-7 col-xs-12" name="email" id="idcard" value="<?php echo $user['Username']?>" /></a></td>
                      </tr>
                        <td>Phone Number</td>
                        <td><input type="text" class="form-control col-md-7 col-xs-12" name="phonenumber" id="phonenumber" value="<?php echo $user['MobileNumber']?>"/>
                        </td>
                      </tr>
                    </tbody>
                                   <table>
                  <div class="actionBar">
                  <button href="<?php echo base_url()?>ipsite/home/edit_user" class="btn btn-primary buttonDisabled" style="width:150px !important;">Edit</a>
                  <button type="submit" class="btn btn-success" style="width:150px !important;">Finish</button>
                  </div>
                </table>
              </table>
                </form>

                   <?php  echo form_close(); ?> 
                </div>
                 
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
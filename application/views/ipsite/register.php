<h2 align="center" style="padding-top:100px;"><strong>Registration Form</strong></h2>
<p align="center" style="margin-top:-50px;">Please use the form below to create an account</p>
   <?php echo form_open('ipsite/user/add_user'); ?>
<form role="form" action="" method="post">
 <div style="padding-left:200px; padding-right:200px; padding-bottom:100px;">
  <div class="form-group">
    <label for="idcard">ID Card</label>
    <input type="text" class="form-control" id="idcard" name="idcard" placeholder="ID Card Number...">
  </div>
  <div class="form-group">
    <label for="email">Email</label>
    <input type="email" class="form-control" id="email" name="email" placeholder="Email Address...">
  </div>
    <div class="form-group">
    <label for="password">Password</label>
    <input type="password" class="form-control" id="passwd" name="passwd">
  </div>
    <div class="form-group">
    <label for="firstname">First Name</label>
    <input type="firstname" class="form-control" id="firstname" name="firstname"  placeholder="First Name..">
  </div>
    <div class="form-group">
    <label for="lastname">Last Name</label>
    <input type="lastname" class="form-control" id="lastname" name="lastname" placeholder="Last Name...">
  </div>
    <div class="form-group">
    <label for="date">Birth Date</label>
    <input type="text" class="form-control" id="date" name="date" placeholder="YYYY/MM/DD">
  </div>
   <div class="form-group">
    <label for="gender">Gender</label>
     <br>
     <?php echo form_dropdown('gender', array('Please Select Gender'=>'Please Select Gender','Male'=>'Male','Female'=>'Female'), '', 'id="city_id" class="input-sm" style="color:#000;"' ); ?>
  </div>
     <div class="form-group">
    <label for="family">Family</label><br>
    <?php echo form_dropdown('family', array('Please Select Family'=>'Please Select Family','SingleFamily'=>'Single Family','QtyFamily'=>'Qty Family'), '', 'id="city_id" class="input-sm" style="color:#000;"' ); ?>
  </div>
   <div class="form-group">
    <label for="qtyfamily">Qty Family</label>
    <p style="font-size:12px;">*If you choose Qty Family please fill this form !</p>
    <input type="text" class="form-control" id="qtyfamily" name="qtyfamily" placeholder="Qty Family....">
  </div>
     <div class="form-group">
    <label for="phonenumber">Phone Number</label>
    <input type="text" class="form-control" id="phonenumber" name="phonenumber" placeholder="Phonenumber....">
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</div>
</form>
<?php echo form_close(); ?>  

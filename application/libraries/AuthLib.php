<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Authlib
{
    var $CI;
    function Authlib()
    {
        $this->CI=& get_instance();
        $this->CI->load->helper('security');
        $this->CI->load->library('session');
    }
    
    public function _prep_password($passwd)
    {
        $passwd = do_hash($passwd, 'md5');
        $passwd = $passwd.$this->CI->config->item('encryption_key');
        
        return $passwd;
    }

    public function try_login($username, $passwd)
    {
        $this->CI->db->where('username', $username);
        $this->CI->db->where('password', $passwd);
        $query = $this->CI->db->get('tb_contestan');
        
        if ($query->num_rows() != 1)
        {
          return FALSE;
        }else{
            $row = $query->row();
            $newdata = array(
                'id'         => $row->ID_Contest,
                'firstname'   => $row->FirstName,
                'lastname'  => $row->LastName,
                'username'   => $row->Username,
                'status'       => $row->Status,
                'timestamp' => $row->Time_Stamp,
                'logged_in'  => TRUE,
            );
            $this->CI->session->set_userdata($newdata);
            return TRUE;
        }
    }

    public function try_schedule_login($username, $passwd, $club_id)
    {
        $this->CI->db->select('*');
        $this->CI->db->where('cf_user.cf_username', $username);
        $this->CI->db->where('cf_user.cf_passwd', $this->_prep_password($passwd));
        $this->CI->db->where('cf_user.club_id', $club_id);
        $this->CI->db->join('country', 'country.country_id = cf_user.country_id');
        $this->CI->db->join('city', 'city.city_id = cf_user.city_id');
        $this->CI->db->join('club', 'club.club_id =cf_user.club_id');

        $query = $this->CI->db->get('cf_user');
        
        if ($query->num_rows() != 1)
        {
          return FALSE;
        }else{
            $row = $query->row();
            $newdata = array(
                'cf_user_id'    => $row->cf_user_id,
                'cf_username'   => $row->cf_username,
                'country'       => $row->country_name,
                'city_id'       => $row->city_id,
                'city_name'     => $row->city_name, 
                'club_id'       => $row->club_id,
                'club_name'     => $row->club_name,
                'email'         => $row->cf_email,
                'last_login'    => $row->last_login,
                'logged_in'     => TRUE,
            );
            $this->CI->session->set_userdata($newdata);
            return TRUE;
        }
    }


    
    public function generate_key($length=9,$chars='0123456789')
    {
      $allowed_count = strlen($chars);
      $alpha_numeric = null;
      $alpha_numeric = '';
      for($i=0;$i < $length;++$i){
        $alpha_numeric.= $chars{mt_rand(0,$allowed_count - 1)};
      }
      
      return strtoupper($alpha_numeric);
    }
    
    public function save_login_date($username)
    {
        $datestring = "%Y-%m-%d %h:%i:%a";
        $time = time();
        $now  = mdate($datestring, $time);
        
        $newdata = array(
            'Time_Stamp' => $now,
        );
        
        $this->CI->db->where('username', $username);
        $query = $this->CI->db->update('tb_contestan', $newdata);
        
        return $query;
    }

    public function save_schedule_login_date($username)
    {
        $datestring = "%Y-%m-%d %h:%i:%a";
        $time = time();
        $now  = mdate($datestring, $time);
        
        $newdata = array(
            'last_login' => $now,
        );
        
        $this->CI->db->where('cf_username', $username);
        $query = $this->CI->db->update('cf_user', $newdata);
        
        return $query;
    }

    public function save_change_user($id, $username, $passwd, $singlefamily, $qtyfamily,$phonenumber)
    {
        

        $newdata = array(
            'Username' => $username,
            'Password'  => $passwd,
            'SinggleFamily' => $singlefamily,
            'QtyFamily' => $qtyfamily,
            'MobileNumber' => $phonenumber
        );

        $this->CI->db->where('ID_Contest', $id);

        $query = $this->CI->db->update('tb_contestan', $newdata);



    }
}
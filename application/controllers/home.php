<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
	    parent::__construct();
	 //   $this->load->model('frontend_model');
	  //  $this->load->library('session');
	    //$this->output->enable_profiler(TRUE);
	}
	
	public function index()
	{
		$this->template->set('title', 'Celebrity Fitness');
		$this->template->load('ipadmin/new_template', 'ipadmin/new_home');
	}
}
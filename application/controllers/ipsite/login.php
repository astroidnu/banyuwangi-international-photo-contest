<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct()
    {
        parent::__construct();
		 
    }
    
    public function index()
	{
		$data['title']= "International Photocontest"; 
        
        $this->load->vars($data);
        
        $this->load->view('ipsite/login');
	}

     public function post(){
        if($this->session->userdata('username_session'))
        {
            redirect('/ipsite');
        }
           
        $email = $this->input->post('txt_email');
        $password = $this->input->post('passwd');

        $this->load->model('Auth');
        $result = $this->Auth->validate_login($email,$password);
           
           //echo $result;
           //echo $this->session->userdata('username_session');
               
           if($this->session->userdata('username_session'))
         {
            if( $this->session->userdata('username_group') == 1 ) {
                redirect('/websoul');
            }else{
                redirect('/');  
            }
         }else{
            redirect('/websoul/login');                    
         }
    }
}

/* End of file cms.php */
/* Location: ./application/controllers/cfadmin/cms.php */
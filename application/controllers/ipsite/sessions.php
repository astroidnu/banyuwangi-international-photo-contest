<?php

class Sessions extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->library('Authlib');
        $this->load->library('session');
    }

    function login()
    {
        $data['title']= "International Photocontest"; 
        
        $this->form_validation->set_rules('username', 'Username', 'required|xss_clean|callback_username_check');
        $this->form_validation->set_rules('password', 'Password', 'required|xss_clean');
        
         if ($this->form_validation->run() == FALSE)
         {
             $this->load->vars($data);
             $this->load->view('ipsite/login');
         }  else {
             $username = $this->session->userdata('username');
             
             $this->authlib->save_login_date($username);
             redirect('dashboard', "refresh");
         }
    }

    public function username_check($username)
    {
        $username = $this->input->post('username');
        $passwd   = $this->input->post('password');
        
        if ($this->authlib->try_login($username, $passwd))
        {
            return TRUE;
        }else{
            $this->form_validation->set_message('username_check', 'Incorrect login info.');
            return FALSE;
        }
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('ipsite/login', 'refresh');
    }
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
	    parent::__construct();
	  	$this->load->model('frontend_model_user');
	    $this->load->library('session');
	    //$this->output->enable_profiler(TRUE);
	}
	
	public function index()
	{
		$data['title']= "International Photocontest"; 
		//getting user data info via session
		 $data['users'] = $this->frontend_model_user->get_detail_user($this->session->userdata('id'));
		//load var data for binding to html
		$this->load->vars($data);
		//setting template and title for website
		$this->template->set('title', 'Celebrity Fitness');
		$this->template->load('ipsite/template_user', 'ipsite/dashboard');


	}
	public function dashboard()
	{
		$data['title']= "International Photocontest"; 
		//getting user data info via session
		 $data['users'] = $this->frontend_model_user->get_detail_user($this->session->userdata('id'));
		//load var data for binding to html
		$this->load->vars($data);

		$this->template->set('title', 'International Traveller Photocontest');
		$this->template->load('ipsite/template_user', 'ipsite/dashboard');


	}

	public function profile()
	{
		$data['title']= "International Photocontest"; 
		//getting user data info via session
		 $data['users'] = $this->frontend_model_user->get_detail_user($this->session->userdata('id'));
		//load var data for binding to html
		$this->load->vars($data);
		// $this->load->vars($data);
		$this->template->set('title', 'International Traveller Photocontest');
		$this->template->load('ipsite/template_user', 'ipsite/profile');


	}
	public function edit_user()
	{
		//getting user data info via session
		 $data['users'] = $this->frontend_model_user->get_detail_user($this->session->userdata('id'));
		//load var data for binding to html
		$this->load->vars($data);
		// $this->load->vars($data);
		$this->template->set('title', 'International Traveller Photocontest');
		$this->template->load('ipsite/template_user', 'ipsite/edit_user');


	}

	public function update_user()
	{
		$data['title']= "International Photocontest"; 
			//getting user data info via session
			$data['users'] = $this->frontend_model_user->get_detail_user($this->session->userdata('id'));
		
			$id = $this->session->userdata('id');
			$username = $this->input->post('username');
			$passwd = $this->input->post('passwd');
			$singlefamily = $this->input->post('singlefamily');
			$qtyfamily = $this->input->post('qtyfamily');
			$phonenumber = $this->input->post('phonenumber');
			print_r($username);
			$this->frontend_model_user->update_user($id, $username, $passwd, $singlefamily, $qtyfamily,$phonenumber);

			redirect('user/profile');
		
		

	}
	public function upload_user()
	{
		//getting user data info via session
		 $data['users'] = $this->frontend_model_user->get_detail_user($this->session->userdata('id'));
		//load var data for binding to html
		$this->load->vars($data);
		$this->template->set('title', 'International Traveller Photocontest');
		$this->template->load('ipsite/template_user', 'ipsite/upload_user');


	}

}

/* End of file home.php */
/* Location: ./application/controllers/cfsite/home.php */
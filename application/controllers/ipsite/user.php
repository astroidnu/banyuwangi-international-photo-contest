<?php

class user extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('frontend_model_user');
        $this->load->library('session');
    }

    function register ()
    {
        $data['title']= "International Photocontest"; 
        

        $this->load->vars($data);
        $this->template->load('ipsite/template','ipsite/register');
    }

    function add_user()
    {

        $idcard = $this->input->post('idcard'); 
        $username = $this->input->post('email');
        $passwd = $this->input->post('passwd');
        $firstname = $this->input->post('firstname');
        $lastname = $this->input->post('lastname');
        $date = $this->input->post('date');
        $gender= $this->input->post('gender');
        if($gender="Male"){
            $gender_baru = 1;
        }else{
            $gender_baru = 0;
        }
        $family= $this->input->post('family');
        if($family=="SingleFamily"){
            $family_baru = 1;
        }else{
            $family_baru = 0;
        }
        $qtyfamily = $this->input->post('qtyfamily');
        $phonenumber= $this->input->post('phonenumber');

        $this->frontend_model_user->add_user_model($idcard, $username, $passwd, $firstname, $lastname,$date, $gender_baru,$family_baru, $qtyfamily,$phonenumber);

        redirect('ipsite/login');   

    }

}
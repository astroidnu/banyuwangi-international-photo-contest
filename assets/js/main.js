jQuery(document).ready(function() {
    
    //set windows height
    function setHeight() {
        windowHeight = jQuery(window).innerHeight();
        paddingTop =  jQuery(window).innerHeight()-((jQuery(window).innerHeight()/2)-90);
        jQuery('#home').css('min-height', windowHeight);
        //jQuery('.header').css('min-height', windowHeight);
        //jQuery('.header').css('padding-top', paddingTop);
        jQuery('.section').css('min-height', windowHeight);
        //jQuery('.section-hover').css('min-height', windowHeight);
        //alert(paddingText);
        
    };
    setHeight();
  
    jQuery(window).resize(function() {
        setHeight();
    });
    
    jQuery(".player").YTPlayer();
    
    //pit bar
	var delta = 5;
	var lastScrollTop = 0;
	var navbar = document.querySelector('.navbar');
                                    
	window.addEventListener('scroll', function (event) {
  		var st = window.pageYOffset;

  		if (Math.abs(lastScrollTop - st) > delta) {
    	
    		if (st > lastScrollTop) {
      			navbar.classList.add("hidden");
    		} else if (st < lastScrollTop) {
      			navbar.classList.remove("hidden");
    		}

    		lastScrollTop = st;
  		}

	});
    
    jQuery('#nav-expander').on('click',function(e){
        e.preventDefault();
        jQuery('body').toggleClass('nav-expanded');
    });
    
    jQuery('#nav-close').on('click',function(e){
        e.preventDefault();
        jQuery('body').removeClass('nav-expanded');
    });

    // Initialize navgoco with default options
    jQuery(".main-menu").navgoco({
        caret: '&nbsp;&nbsp;<span class="caret "></span>',
        accordion: false,
        openClass: 'open',
        save: true,
        cookie: {
            name: 'navgoco',
            expires: false,
            path: '/'
        },
        slide: {
            duration: 300,
            easing: 'swing'
        }
    }); // Pit bar end
    
    jQuery('.section-hover .thumbnail').hover(
        function(){
            jQuery(this).find('.caption').slideDown(250); //.fadeIn(250)
        },
        function(){
            jQuery(this).find('.caption').slideUp(250); //.fadeOut(205)
        }
    ); 

	jQuery('.bxslider').bxSlider(
      {
        controls: false,
        slideWidth: 200,
        minSlides: 2,
        maxSlides: 2,
        slideMargin: 10  
      }
    );
    
    var $box = $('.box').css('width');
    var $img = $('.box img').width();
    var $gox = (($img/2 - (parseInt($box, 10) / 2)) + 'px');
    $('.box img').css('marginLeft', '-'+$gox);


    //Just the same code as above, but with window.onresize
    window.onresize = function(event){
        var $box = $('.box').css('width');
        var $img = $('.box img').width();
        var $gox = (($img/2 - (parseInt($box, 10) / 2)) + 'px');
        $('.box img').css('marginLeft', '-'+$gox);
    };
    
    var bodyHeight = $("body").height();
    var vwptHeight = $(window).height();
    if (vwptHeight > bodyHeight) {
        $(".footer").css("position","absolute").css("bottom",0);
    }
	
});
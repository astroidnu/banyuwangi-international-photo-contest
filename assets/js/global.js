jQuery(document).ready(function() {
    
    //set windows height
    function setHeight() {
        windowHeight = jQuery(window).innerHeight();
        paddingTop =  jQuery(window).innerHeight()-((jQuery(window).innerHeight()/1.5))+8;
        jQuery('#home').css('min-height', windowHeight);
        jQuery('#header-banner').css('min-height', windowHeight);
        //jQuery('#header-group-fitness').css('min-height', windowHeight);
        //jQuery('#header-personal-training').css('min-height', windowHeight);
        //jQuery('#header-club-page').css('min-height', windowHeight);
        jQuery('#pick-your-themes').css('min-height', windowHeight);
        jQuery('#about-us').css('min-height', windowHeight);
        //jQuery('#header-everyday-i-get-better').css('min-height', windowHeight);
        jQuery('#press').css('min-height', windowHeight);
        // jQuery('#career').css('min-height', windowHeight);
        //jQuery('.header').css('padding-top', paddingTop);


        
    };
    setHeight();
  
    jQuery(window).resize(function() {
        setHeight();
    });
    
    jQuery(".player").YTPlayer();
    
    //pit bar
	var delta = 5;
	var lastScrollTop = 0;
	var navbar = document.querySelector('.navbar');
                                    
	window.addEventListener('scroll', function (event) {
  		var st = window.pageYOffset;

  		if (Math.abs(lastScrollTop - st) > delta) {
    	
    		if (st > lastScrollTop) {
      			navbar.classList.add("hidden");
    		} else if (st < lastScrollTop) {
      			navbar.classList.remove("hidden");
    		}

    		lastScrollTop = st;
  		}

	});
    
    jQuery('#nav-expander').on('click',function(e){
        e.preventDefault();
        jQuery('body').toggleClass('nav-expanded');
    });
    
    jQuery('#nav-close').on('click',function(e){
        e.preventDefault();
        jQuery('body').removeClass('nav-expanded');
    });

    // Initialize navgoco with default options
    jQuery(".main-menu").navgoco({
        caret: '&nbsp;&nbsp;<span class="caret "></span>',
        accordion: false,
        openClass: 'open',
        save: true,
        cookie: {
            name: 'navgoco',
            expires: false,
            path: '/'
        },
        slide: {
            duration: 300,
            easing: 'swing'
        }
    }); // Pit bar end
    
//    jQuery('.section-hover .thumbnail').hover(
//        function(){
//            jQuery(this).find('.caption').slideDown(250); //.fadeIn(250)
//        },
//        function(){
//            jQuery(this).find('.caption').slideUp(250); //.fadeOut(205)
//        }
//    ); 

	jQuery('.bxslider').bxSlider(
      {
        controls: false,
        slideWidth: 200,
        minSlides: 4,
        maxSlides: 4,
        slideMargin: 10  
      }
    );
    
//    var $box = $('.box').css('width');
//    var $img = $('.box img').width();
//    var $gox = (($img/2 - (parseInt($box, 10) / 2)) + 'px');
//    $('.box img').css('marginLeft', '-'+$gox);
//
//
//    //Just the same code as above, but with window.onresize
//    window.onresize = function(event){
//        var $box = $('.box').css('width');
//        var $img = $('.box img').width();
//        var $gox = (($img/2 - (parseInt($box, 10) / 2)) + 'px');
//        $('.box img').css('marginLeft', '-'+$gox);
//    };
    
    $("[rel='tooltip']").tooltip();    
 
    $('.thumbnail').hover(
        function(){
            $(this).find('.caption').slideUp(250); //.fadeIn(250)
        },
        function(){
            $(this).find('.caption').slideDown(250); //.fadeOut(205)
        }
    ); 
    
    
    // Sticky Footer
    
    var bodyHeight = $("body").height();
    var vwptHeight = $(window).height();
    if (vwptHeight > bodyHeight) {
        $("footer#sticky").css("position","absolute").css("bottom",0);
    }

   
    /* Back to Top */
    if ( (jQuery(window).height() + 100) < jQuery(document).height() ) {
      jQuery('#top-link-block').removeClass('hidden').affix({
          offset: {top:100}
      });
    }
	
});
